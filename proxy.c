/*
 * proxy.c - CS:APP Web proxy
 *
 * TEAM MEMBERS:
 *     Sang-goo Ahn, ahn@insideMen.com
 *     Jang-hoon Woo, woo@insideMen.com
 * 
 * IMPORTANT: Give a high level description of your code here. You
 * must also provide a header comment at the beginning of each
 * function that describes what that function does.
 */ 

#include "csapp.h"

#define MAX_CACHE_SIZE 1049000
#define MAX_OBJECT_SIZE 102400

// struct definitions
struct arg_struct {
    int fd;
    socklen_t socklen;
    struct sockaddr_in *sockaddr;
};

struct cache_obj {
    int size; // size of content member variable
    char key[MAXLINE];
    char content[MAX_OBJECT_SIZE];
    struct cache_obj *prev;
    struct cache_obj *next;
};

struct cache {
    int size; // size of total cache object contents
    struct cache_obj *begin;
    struct cache_obj *end;
};



// function prototypes for cache
void cache_init(struct cache *c);
int cache_empty(struct cache *c);
struct cache_obj *cache_remove(struct cache *c, struct cache_obj *obj);
struct cache_obj *cache_pop(struct cache *c);
void cache_push(struct cache *c, struct cache_obj *obj);
void cache_use(struct cache *c, struct cache_obj *obj);
struct cache_obj *cache_find(struct cache *c, char *key, int key_size);

/*
 * Function prototypes
 */
int parse_uri(char *uri, char *target_addr, char *path, int  *port);
void format_log_entry(char *logstring, struct sockaddr_in *sockaddr, 
                    char *uri, int size);
ssize_t Rio_readnb_w(rio_t *rp, void *usrbuf, size_t n);
ssize_t Rio_readnb_w_timeout(rio_t *rp, void *usrbuf, size_t n);
ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen);
ssize_t Rio_readlineb_w_timeout(rio_t *rp, void *usrbuf, size_t maxlen);
void Rio_writen_w(int fd, void *usrbuf, size_t n);
int open_clientfd_timeout(char *hostname, int port, struct timeval *tv);
int _Open_clientfd(char *hostname, int port);
struct hostent *gethostbyname_ts(char *hostname, struct hostent *privatep);
void get_hostname_from_hdr(rio_t *rp, char *hostname);
void read_requesthdrs(rio_t *rp, char *hostname);
char *ltrim(char *s);
void proxy(int user_fd, struct sockaddr_in *user_sockaddr);
void *thread(void *vargp);

// global variables
struct cache cache;
sem_t mutex_log;
sem_t mutex_gethostbyname;
sem_t mutex_cache;
pthread_rwlock_t rwlock;

/* 
 * main - Main routine for the proxy program 
 */
int main(int argc, char **argv)
{
    int listenfd, connfd, port;
    socklen_t clientlen = sizeof(struct sockaddr_in);
    struct sockaddr_in *clientaddr;

    pthread_t tid;
    struct arg_struct *args;

    /* Check arguments */
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <port number>\n", argv[0]);
        exit(0);
    }
    Signal(SIGPIPE, SIG_IGN);
    cache_init(&cache);
    Sem_init(&mutex_log, 0, 1);
    Sem_init(&mutex_gethostbyname, 0, 1);
    // TODO: make wrapper function: if init lock fails, end process.
    pthread_rwlock_init(&rwlock, NULL);
    port = atoi(argv[1]);

    listenfd = Open_listenfd(port);
    printf("proxy server started running (pid: %d)\n", getpid());
    printf("proxy server listening on port %d\n", port);
    while (1) {
        clientaddr = Malloc(sizeof(struct sockaddr_in));
        args = Malloc(sizeof(struct arg_struct));
        connfd = Accept(listenfd, (SA *) clientaddr, &clientlen);
        args->fd = connfd;
        args->socklen = clientlen;
        args->sockaddr = clientaddr; 
        Pthread_create(&tid, NULL, thread, (void *)args);
    }

    exit(0);
}

void *thread(void *vargp)
{
    int connfd;
    struct sockaddr_in clientaddr;
    struct arg_struct *args = (struct arg_struct *)vargp;
    connfd = args->fd;
    clientaddr.sin_family = args->sockaddr->sin_family;
    clientaddr.sin_port = args->sockaddr->sin_port;
    clientaddr.sin_addr = args->sockaddr->sin_addr;
    memset(clientaddr.sin_zero, 0, 8);
    Pthread_detach(pthread_self());
    Free(args->sockaddr);
    Free(vargp);
    proxy(connfd, &clientaddr);
    Close(connfd);
    return NULL;
}

// functions for cache
void cache_init(struct cache *c) 
{
    struct cache_obj *begin = Malloc(sizeof(struct cache_obj));
    struct cache_obj *end = Malloc(sizeof(struct cache_obj));
    memset(begin, 0, sizeof(struct cache_obj));
    memset(end, 0, sizeof(struct cache_obj));
    begin->next = end;
    end->prev = begin;

    memset(c, 0, sizeof(struct cache));
    c->begin = begin;
    c->end = end;
}

int cache_empty(struct cache *c)
{
    return (c->size == 0);
}

struct cache_obj *cache_remove(struct cache *c, struct cache_obj *obj)
{
    if (obj->prev == NULL || obj->next == NULL) return NULL;
    obj->prev->next = obj->next;
    obj->next->prev = obj->prev;
    obj->next = NULL;
    obj->prev = NULL;
    
    c->size -= obj->size;
    return obj;
}

// pop is done at the back.
struct cache_obj *cache_pop(struct cache *c)
{
    // check if empty
    if (cache_empty(c)) return NULL;
    return cache_remove(c, c->end->prev);
}

// push is done at the front
void cache_push(struct cache *c, struct cache_obj *obj)
{
    if (obj == NULL) return;

    while (c->size + obj->size > MAX_CACHE_SIZE) {
        Free(cache_pop(c));
    }
    struct cache_obj *front = c->begin->next;
    obj->prev = c->begin;
    obj->next = front;
    c->begin->next = obj;
    front->prev = obj;
    
    c->size += obj->size;
}

void cache_use(struct cache *c, struct cache_obj *obj)
{
    if (obj == NULL) return NULL;
    cache_push(c, cache_remove(c, obj));
}

struct cache_obj *cache_find(struct cache *c, char *key, int key_size)
{
    struct cache_obj *obj_iter;
    for (obj_iter = c->begin->next; obj_iter != c->end; obj_iter = obj_iter->next) {
        if (strncmp(obj_iter->key, key, key_size) == 0) {
            return obj_iter;
        }
    }
    return NULL;
}


/*
 * parse_uri - URI parser
 * 
 * Given a URI from an HTTP proxy GET request (i.e., a URL), extract
 * the host name, path name, and port.  The memory for hostname and
 * pathname must already be allocated and should be at least MAXLINE
 * bytes. Return -1 if there are any problems.
 */
int parse_uri(char *uri, char *hostname, char *pathname, int *port)
{
    char *hostbegin;
    char *hostend;
    char *pathbegin;
    int len;

    if (strncasecmp(uri, "http://", 7) != 0) {
        hostname[0] = '\0';
        return -1;
    }
       
    /* Extract the host name */
    hostbegin = uri + 7;
    // hostend = strpbrk(":", hostbegin);
    // if (hostend == 0) hostend = strpbrk("/", hostbegin);
    // if (hostend == 0) hostend = hostbegin + strlen(hostbegin);
    hostend = strpbrk(hostbegin, " :/\r\n\0");
    if (hostend == 0) hostend = hostbegin + strlen(hostbegin);
    len = hostend - hostbegin;
    strncpy(hostname, hostbegin, len);
    hostname[len] = '\0';
    
    /* Extract the port number */
    *port = 80; /* default */
    if (*hostend == ':') {
        *port = atoi(hostend + 1);
    }
    
    /* Extract the path */
    pathbegin = strchr(hostbegin, '/');
    if (pathbegin == NULL) {
        pathname[0] = '/';
        // pathname[0] = '\0';
    }
    else {
        // pathbegin++;	
        strcpy(pathname, pathbegin);
    }

    return 0;
}


/*
 * format_log_entry - Create a formatted log entry in logstring. 
 * 
 * The inputs are the socket address of the requesting client
 * (sockaddr), the URI from the request (uri), and the size in bytes
 * of the response from the server (size).
 */
void format_log_entry(char *logstring, struct sockaddr_in *sockaddr, 
		      char *uri, int size)
{
    time_t now;
    char time_str[MAXLINE];
    unsigned long host;
    unsigned char a, b, c, d;

    /* Get a formatted time string */
    now = time(NULL);
    strftime(time_str, MAXLINE, "%a %d %b %Y %H:%M:%S %Z", localtime(&now));

    /* 
     * Convert the IP address in network byte order to dotted decimal
     * form. Note that we could have used inet_ntoa, but chose not to
     * because inet_ntoa is a Class 3 thread unsafe function that
     * returns a pointer to a static variable (Ch 13, CS:APP).
     */
    host = ntohl(sockaddr->sin_addr.s_addr);
    a = host >> 24;
    b = (host >> 16) & 0xff;
    c = (host >> 8) & 0xff;
    d = host & 0xff;


    /* Return the formatted log entry string */
    sprintf(logstring, "%s: %d.%d.%d.%d %s %d", time_str, a, b, c, d, uri, size);
}

ssize_t Rio_readnb_w(rio_t *rp, void *usrbuf, size_t n) 
{
    ssize_t rc;

    if ((rc = rio_readnb(rp, usrbuf, n)) < 0) {
        printf("%s: %s\n", "Rio_readnb_w error", strerror(errno));
        return 0;
    }
    return rc;
}

ssize_t Rio_readnb_w_timeout(rio_t *rp, void *usrbuf, size_t n) 
{
    ssize_t rc;

    if ((rc = rio_readnb(rp, usrbuf, n)) < 0 || errno == EAGAIN || errno == EWOULDBLOCK) {
        printf("%s: %s\n", "Rio_readnb_w_timeout error", strerror(errno));
        return 0;
    }
    return rc;
}

ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen) 
{
    ssize_t rc;

    if ((rc = rio_readlineb(rp, usrbuf, maxlen)) < 0) {
        printf("\n%s: %s\n", "Rio_readlineb_w error", strerror(errno));
        return 0;
    }
    return rc;
} 

// only usable when SO_RCVTIMEO option is set to socket fd.
ssize_t Rio_readlineb_w_timeout(rio_t *rp, void *usrbuf, size_t maxlen) 
{
    ssize_t rc;

    if ((rc = rio_readlineb(rp, usrbuf, maxlen)) < 0 || errno == EAGAIN || errno == EWOULDBLOCK) {
        printf("\n%s: %s\n", "Rio_readlineb_w_timeout error", strerror(errno));
        return 0;
    }
    return rc;
} 

void Rio_writen_w(int fd, void *usrbuf, size_t n) 
{
    if (rio_writen(fd, usrbuf, n) != n) {
        printf("\ntried to write: %s\n%s: %s\n", (char *)usrbuf, "Rio_writen_w error", strerror(errno));
        return;
    }
}

int _Open_clientfd(char *hostname, int port)
{
    int rc;

    if ((rc = open_clientfd(hostname, port)) < 0) {
	if (rc == -1)
	    unix_error("Open_clientfd Unix error");
	else        
	    dns_error("Open_clientfd DNS error");
    }
    return rc;
}

struct hostent *gethostbyname_ts(char *hostname, struct hostent *privatep) {
    struct hostent *sharedp;
    
    // sema down
    P(&mutex_gethostbyname);
    sharedp = gethostbyname(hostname);
    memcpy(privatep, sharedp, sizeof(struct hostent));
    V(&mutex_gethostbyname);
    // sema up
    return privatep;
}

int open_clientfd_timeout(char *hostname, int port, struct timeval *tv)
{
    int clientfd;
    struct hostent h;
    struct hostent *hp;
    struct sockaddr_in serveraddr;

    hp = &h;

    if ((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1; /* check errno for cause of error */
    }

    /* Fill in the server's IP address and port */
    if ((hp = gethostbyname_ts(hostname, hp)) == NULL) {
        return -2; /* check h_errno for cause of error */
    }
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)hp->h_addr_list[0], 
	  (char *)&serveraddr.sin_addr.s_addr, hp->h_length);
    serveraddr.sin_port = htons(port);

    // set timeout option
    if (setsockopt(clientfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)tv,sizeof(struct timeval)) == -1) {
        return -1;
    }

    /* Establish a connection with the server */
    if (connect(clientfd, (SA *) &serveraddr, sizeof(serveraddr)) < 0) {
        return -1;
    }
    return clientfd;
}

void get_hostname_from_hdr(rio_t *rp, char *hostname)
{
    char buf[MAXLINE];
    char key[MAXLINE], value[MAXLINE];
    
    Rio_readlineb_w(rp, buf, MAXLINE);
    while(strcmp(buf, "\r\n")) {
        // if key is Host, store that to hostname
        sscanf(buf, "%s: %s", key, value);
        if (strcmp(key, "Host")) {
            memcpy(hostname, value, strlen(value) + 1);
        }
        Rio_readlineb_w(rp, buf, MAXLINE);
    }
}

void read_requesthdrs(rio_t *rp, char *hostname)
{
    char buf[MAXLINE];
    // char key[MAXLINE], value[MAXLINE];
    
    Rio_readlineb_w(rp, buf, MAXLINE);
    while(strcmp(buf, "\r\n")) {
        // sscanf(buf, "%s: %s", key, value);
        // if (strcmp(key, "Host") == 0) {
        //     memcpy(hostname, value, strlen(value)+1);
        // }
        Rio_readlineb_w(rp, buf, MAXLINE);
    }
}

// 문자열 좌측 공백문자 삭제 함수
char* ltrim(char *s) {
  char* begin;
  begin = s;

  while (*begin != '\0') {
    if (isspace(*begin))
      begin++;
    else {
      s = begin;
      break;
    }
  }

  return s;
}

void proxy(int user_fd, struct sockaddr_in *user_sockaddr)
{
    size_t n;
    char buf[MAXLINE], logbuf[MAXLINE];
    char uri[MAXLINE], method[MAXLINE], version[MAXLINE];
    char hostname[MAXLINE], pathname[MAXLINE];
    int web_port, web_fd;
    int content_len, recv_size, header_size, body_size;
    rio_t user_rio, web_rio;

    // variables for timeout
    struct timeval tv;
    fd_set readfds;
    int state;

    // variables for cache
    struct cache_obj *obj;


    // initializations
    Rio_readinitb(&user_rio, user_fd);
    recv_size = 0;
    header_size = 0;
    body_size = 0;
    content_len = 0;

    // wait for browser's input for 10 seconds
    FD_ZERO(&readfds);
    FD_SET(user_fd, &readfds);
    tv.tv_sec = 20;
    tv.tv_usec = 20000000;
    state = Select(user_fd+1, &readfds, (fd_set *)0, (fd_set *)0, &tv);
    if (state == -1) {
        return;
    } else if (state == 0) {
        return;
    }

    // read http request line
    if ((n = Rio_readlineb_w(&user_rio, buf, MAXLINE)) <= 0) {
        // closing is done in main().
        return;
    }

    // check cache before connecting to the web.
    pthread_rwlock_rdlock(&rwlock);
    if ((obj = cache_find(&cache, buf, n)) != NULL) {
        pthread_rwlock_unlock(&rwlock);

        // send obj
        Rio_writen_w(user_fd, obj->content, obj->size);

        pthread_rwlock_wrlock(&rwlock);
        cache_use(&cache, obj);
        pthread_rwlock_unlock(&rwlock);
        return;
    }
    pthread_rwlock_unlock(&rwlock);

    // initialize cache_obj
    obj = Malloc(sizeof(struct cache_obj));
    memset(obj, 0, sizeof(struct cache_obj));
    obj->size = 0;
    strncpy(obj->key, buf, n);

    // open connection to web server
    sscanf(buf, "%s %s %s", method, uri, version);
    parse_uri(uri, hostname, pathname, &web_port);
    web_fd = open_clientfd_timeout(hostname, web_port, &tv);
    if (web_fd < 0) {
        return;
    }


    // send request line
    memset(buf, 0, MAXLINE);
    sprintf(buf, "%s %s HTTP/1.0\r\n", method, pathname); // should forward HTTP/1.1 to HTTP/1.0
    Rio_writen_w(web_fd, buf, strlen(buf));

    // transfer request header
    // host, user-agent, connection, proxy-connection, accept-encoding are fixed.
    memset(buf, 0, MAXLINE);
    sprintf(buf, "Host: %s\r\n", hostname);
    Rio_writen_w(web_fd, buf, strlen(buf));
    Rio_writen_w(web_fd, "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:10.0.3) Gecko/20120305 Firefox/10.0.3\r\n", 86);
    Rio_writen_w(web_fd, "Connection: close\r\n", 19);
    Rio_writen_w(web_fd, "Proxy-Connection: close\r\n", 25);
    Rio_writen_w(web_fd, "Accept-Encoding: identity\r\n", 27);
    
    Rio_readlineb_w(&user_rio, buf, MAXLINE);
    while(strcmp(buf, "\r\n") != 0) {
        if (strcasestr(buf, "host:") == NULL && strcasestr(buf, "user-agent:") == NULL && 
            strcasestr(buf, "connection:") == NULL && strcasestr(buf, "accept-encoding:") == NULL) {
            Rio_writen_w(web_fd, buf, strlen(buf));
        }
        Rio_readlineb_w(&user_rio, buf, MAXLINE);
    }
    Rio_writen_w(web_fd, "\r\n", 2);


    // initialize rio for web transfer
    Rio_readinitb(&web_rio, web_fd);

    // transfer response header, get content-length
    header_size += Rio_readlineb_w(&web_rio, buf, MAXLINE);
    memcpy(obj->content, buf, header_size); obj->size += header_size;
    while(strcmp(buf, "\r\n") != 0) {
        if (strcasestr(buf, "content-length") != NULL) {
            content_len = atoi(ltrim(buf + 15));
        }
        Rio_writen_w(user_fd, buf, strlen(buf));
        header_size += Rio_readlineb_w_timeout(&web_rio, buf, MAXLINE);
        memcpy(obj->content + obj->size, buf, header_size - obj->size); obj->size += header_size - obj->size;
    }

    Rio_writen_w(user_fd, "\r\n", 2);
    header_size += 2;

    // if content length is not set. set it to be largest int.
    if (content_len == 0) {
        content_len = 0x7FFFFFFF;
    }

    // transfer response body
    while (body_size < content_len && (n = Rio_readnb_w_timeout(&web_rio, buf, MAXLINE)) > 0) {
        Rio_writen_w(user_fd, buf, n);
        body_size += n;
        if (obj->size + n <= MAX_OBJECT_SIZE) {
            memcpy(obj->content + obj->size, buf, n);
        }
        obj->size += n;
    }

    // log if there is response
    recv_size = header_size + body_size;
    if (recv_size > 0) {
        if (recv_size <= MAX_OBJECT_SIZE) {
            // store in cache
            pthread_rwlock_wrlock(&rwlock);
            cache_push(&cache, obj);
            pthread_rwlock_unlock(&rwlock);
        } else {
            Free(obj);
        }
        format_log_entry(logbuf, user_sockaddr, uri, recv_size);
        P(&mutex_log);
        FILE *logfile = fopen("proxy.log", "a");
        fprintf(logfile, "%s\n", logbuf);
        fclose(logfile);
        V(&mutex_log);
    }

    // close socket.
    Close(web_fd);
}
